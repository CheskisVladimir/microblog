package mic.model;

import org.springframework.data.annotation.Id;

public class ServiceData {
	@Id
	private int id = 0;

	public ServiceData() {
		super();
	}
	
	public int makeUserId(){
		return nextUserID++;
	}

	public int makePostId(){
		return nextPostID++;
	}
	
	int nextUserID = 0;
	int nextPostID = 0;
}
