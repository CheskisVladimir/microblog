package mic.model;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import mic.entities.Post;
import mic.entities.User;
import mic.entities.Vote;
import mic.repo.PostsRepository;
import mic.repo.ServiceDataRepository;
import mic.repo.UsersRepository;

public class DataModelMongo implements IDataModel {

	
	private static final int INVALID_LOGIN = -1;
	private static final int INVALID_POST = -1;

	@Override
	public int getUserId(String login, String password) {
		List<User> users = usersRepository.findByLogin(login);
		if (users == null || users.size() != 1){
			return INVALID_LOGIN;
		}
		User user = users.get(0);
		return (user.getPassword() == password) ? user.getId() : INVALID_LOGIN; 
	}

	@Override
	public void addUser(String login, String password) {
		initServiceData();
		User user = new User(serviceData.makeUserId(), login, password);
		usersRepository.save(user);
		saveServiceData();
	}
	
	@Override
	public int addPost(int userId, String text) {
		User user = usersRepository.findOne(userId);
		if (user == null){
			return INVALID_POST;
		}
		initServiceData();
		Post post = new Post(serviceData.makePostId(), user, text);
		postsRepository.save(post);
		return 0;
	}

	@Override
	public void vote(int userId, int postId) {
		Post post = postsRepository.findOne(postId);
		if (post != null){
			User user = usersRepository.findOne(userId);
			if (user != null && getVoteState(post, user) == Vote.State.None){
				Vote vote = new Vote(user, Vote.State.Voted);
				post.getVotes().add(vote);
				postsRepository.save(post);
			}
		}
	}

	@Override
	public void deVote(int userId, int postId) {
		Post post = postsRepository.findOne(postId);
		if (post != null){
			User user = usersRepository.findOne(userId);
			if (user != null){
				for (Vote vote : post.getVotes()){
					if (vote.getFrom() == user){
						if (vote.getState() == Vote.State.Voted){
							vote.setState(Vote.State.DeVouted);
							postsRepository.save(post);
							return;
						}
					}
				}
			}
		}
	}

	@Override
	public List<Post> getTop100() {
		// TODO Write Real Code
		return postsRepository.findAll();
	}
	
	Vote.State getVoteState(Post post, User user){
		for (Vote v : post.getVotes()){
			if (v.getFrom() == user){
				return v.getState();
			}
		}
		return Vote.State.None;
	}

	
	@Autowired
	UsersRepository usersRepository;

	@Autowired
	PostsRepository postsRepository;

	@Autowired
	ServiceDataRepository serviceDataRepository;
	
	ServiceData serviceData = null;

	@Override
	public void changePost(int postId, String text) {
		Post post = postsRepository.findOne(postId);
		if (post != null){
			post.setText(text);
			postsRepository.save(post);
		}
		
	}
	
	void initServiceData(){
		if (serviceData == null){
			serviceData = serviceDataRepository.findOne(0);
			if (serviceData == null){
				serviceData = new ServiceData();
				serviceDataRepository.save(serviceData);
			}
		}
	}
	void saveServiceData(){
		serviceDataRepository.save(serviceData);
	}
}
