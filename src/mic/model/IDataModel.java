package mic.model;

import java.util.List;

import mic.entities.Post;

public interface IDataModel {
	
	void addUser(String login, String password);
	int getUserId(String login, String password);
	
	int addPost(int userId, String text);
	void changePost(int postId, String text);
	void vote(int userId, int postId);
	void deVote(int userId, int postId);

	List<Post> getTop100();
}
