package mic.repo;

import java.util.stream.Stream;

import org.springframework.data.mongodb.repository.MongoRepository;

import mic.model.ServiceData;

public interface ServiceDataRepository extends MongoRepository<ServiceData, Integer> {
}
