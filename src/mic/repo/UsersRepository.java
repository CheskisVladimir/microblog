package mic.repo;

import java.util.List;
import java.util.stream.Stream;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import mic.entities.User;

public interface UsersRepository extends MongoRepository<User, Integer> {

	Stream<User> findAllBy();
	List<User>   findByLogin(String login);
}
