package mic.repo;

import java.util.stream.Stream;

import org.springframework.data.mongodb.repository.MongoRepository;

import mic.entities.Post;

public interface PostsRepository extends MongoRepository<Post, Integer> {
	Stream<Post> findAllBy();
}
