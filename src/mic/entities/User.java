package mic.entities;

import org.springframework.data.annotation.Id;

public class User {
	@Id
	private int id;
	String login;
	String password;
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public final int getId() {
		return id;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public User(int id, String login, String password) {
		super();
		this.id = id;
		this.login = login;
		this.password = password;
	}
}
