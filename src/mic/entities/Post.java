package mic.entities;

import java.util.ArrayList;

import org.springframework.data.annotation.Id;

public class Post {
	public void setText(String text) {
		this.text = text;
	}
	public User getAuthor() {
		return author;
	}
	public String getText() {
		return text;
	}
	public ArrayList<Vote> getVotes() {
		return votes;
	}
	public int getRate() {
		return rate;
	}
	public final int getId() {
		return id;
	}
	public Post(int id, User author, String text) {
		super();
		this.id = id;
		this.author = author;
		this.text = text;
	}
	
	@Id
	private int id;
	User author;
	String text;
	ArrayList<Vote> votes = new ArrayList<>(); 
	int  rate = 0;	//total count of votes with state = Voted
}
