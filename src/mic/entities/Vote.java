package mic.entities;

public class Vote {
	public User getFrom() {
		return from;
	}
	public void setFrom(User from) {
		this.from = from;
	}
	public State getState() {
		return state;
	}
	public void setState(State state) {
		this.state = state;
	}
	public Vote(User from, State state) {
		super();
		this.from = from;
		this.state = state;
	}
	public enum State {None, Voted, DeVouted};
	User from;
	State state = State.None;
}
